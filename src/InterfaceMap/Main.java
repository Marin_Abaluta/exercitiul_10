package InterfaceMap;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Main {

    public static void main(String[] args) {

        HashMap<String,Integer> map = new HashMap();

        map.put("Bartenders",5);
        map.put("Receptionists",3);
        map.put("Cashiers",10);
        map.put("Porters",8);

        System.out.println("Total type of hotel employees: " + map.size());

        /*Set entryset = map.entrySet();
        Iterator i = entryset.iterator();

        while (i.hasNext()){

            Map.Entry mentry = (Map.Entry)i.next();
            System.out.println(mentry.getKey() + "-" + mentry.getValue());

        }
        */
        for(String a:map.keySet()){

            System.out.println(a + " - " + map.get(a));
        }



        System.out.println("Found total " +  map.get("Cashiers") + " Cashiers employees!");

        map.clear();

        System.out.println("After clear operation, size is now: " + map.size());



    }
}
